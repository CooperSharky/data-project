﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    // Holds the ground tiles from Tiled. Filled at runtime. 
    public Transform groundHolder;
    // Holds the turret objects from Tiled. Filled at Runtime. 
    public Transform turretHolder;
    // Holds the mine objects from Tiled. Filled at Runtime. 
    public Transform mineHolder;


    // Gearing up to read some Comma Separated Values, and Base64 grid values from Tiled.
    public enum MapDataFormat {
        Base64,
        CSV
    }

    //Allows us to choose Base64 or CSV in the inspector. We'll be using Base64. 
    public MapDataFormat mapDataFormat;

    //The SpriteSheet that our Tiled project uses
    public Texture2D spriteSheetTexture;

    // Our Precious Prefabs in unity! Love them, cherish them. 
    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject minePrefab; 
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> minePositions; 

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    // THE NAME OF THE FILE WE GET FROM TILED - IT'S A TMX 
    public string TMXFilename;

    // These don't appear 
    string gameDirectory;
    string dataDirectory;

    // These do 
    //Path to our maps folder
    string mapsDirectory;
    //Path to our sprite sheet
    string spriteSheetFile;
    //String representing the complete path to our TMX File. 
    string TMXFile;

    //Variables to manually tile up our sprite sheet. 
    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    //A function that... clears the console. 
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    // A function that destroys all children of a parent object. 
    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET
    //READS THE WHOLE FILE, PAINSTAKINGLY, AND THREADS RELEVANT DETAILS INTO THIS SCRIPT. 
    public void LoadLevel() {

        ClearEditorConsole();

        //Clears any older, unwanted children so we can load in shiny new children. 
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        DestroyChildren(mineHolder); 

        // Makes our file pathways good and proper. So we know exactly how to get to the TMX file. 

        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // Read the TMX file. 
        {
            mapData.Clear();

            //Place the file's ascii content in a string
            string content = File.ReadAllText(TMXFile);

            //use XMLReader to make a string reader that reads the string we just made
            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                //Start by reading the file until you hit the string "map" 
                reader.ReadToFollowing("map");

                //Link map rows and columns in unity to the TMX file's width and height attributes 
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                //Read until "tileset"
                reader.ReadToFollowing("tileset");
                //Make sure Unity's tile width and length match the TMX file's attributes for the same. 
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                //Ok I'm starting to sense a pattern here. 
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                //The TMX file's source informs where Unity thinks the SpiteSheetFile path is. 
                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));


                reader.ReadToFollowing("layer");


                reader.ReadToFollowing("data");
                //Find out how the file was encoded. 
                string encodingType = reader.GetAttribute("encoding");

                //Switch this script's encoding type based on the encoding type we got.  
                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                mapDataString = reader.ReadElementContentAsString().Trim();

                minePositions.Clear();
                //Assigning mine positions to our array!
                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            minePositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }


                turretPositions.Clear();
                //Assigning turret positions to our array!
                // THIS IS PROBABLY GONNA MATTER LATER, HEADS UP 
                if (reader.ReadToFollowing("objectgroup")) {
                    if (reader.ReadToDescendant("object")) {
                        do {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }

            }

            //Time to finally do stuff with the encoding type. CSV is gonna need to split up strings, whereas Base64 is basically turning a grid of numbers into tile indexes. 
            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }

        //Telling unity how big tiles should actually be. 
        {

            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // Turning the image into a texture. 
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Generating the actual Map Sprites. 
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // Putting down the tiles in world space, and putting them in our list. 
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }

        // Instantiating our mine prefabs in mine position! 
        {
            foreach (Vector3 minePosition in minePositions)
            {
                GameObject mine = Instantiate(minePrefab, minePosition + mapCenterOffset, Quaternion.identity) as GameObject;
                mine.name = "MINE";
                mine.transform.parent = mineHolder;
            }
        }

        // Instantiating our turret prefabs in turret position! 
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }


        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


