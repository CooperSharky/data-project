﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        //(Make sure there isn't another copy of this script somewhere)
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        //Just gettin the file path based on the hardcoded string at the top 
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }


    public void AddObject(string item) {
        saveItems.Add(item);
    }

    public void Save() {
        //In this case, saving always overwrites. 
        saveItems.Clear();

        //Finds objects that have a save script on them - meaning, any script that inherits from the "Save" type. 
        //This seems to be a shorthand for finding them and putting them straight into an array!
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {

            //SERIALIZE all the objects you've just collected. By Serialize we mean "put in a format Unity stores between runtimes"
            //For what's happening see TankSave *****************************
            saveItems.Add(saveableObject.Serialize());
        }

        //Writing our newly serialized array of items into the game data file! 
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    public void Load() {

        //Reload the scene, clear the saveItems list. 
        //Reloading the scene will trigger OnSceneLoaded

        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }







    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {

        //Don't do anything if we're not loading save. 
        if (firstPlay) return;

        //Populate saveItems with written data
        LoadSaveGameData();

        //Destroy existing object counterparts
        DestroyAllSaveableObjectsInScene();

        //Populate the scene with the saved versions of these objects
        CreateGameObjects();
    }

    void LoadSaveGameData() {

        //Have a reader pass through the game data file and populate the saveItems list
        //Because the load function clears it, the list does indeed need to be repopulated from the file. 

        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            while (gameDataFileStream.Peek() >= 0) {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

    void CreateGameObjects() {

        //Going through the serialized list of data.
        //Determine which gameObject prefab it refers to, and tell it to DESERIALIZE (See specific script)****

        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
