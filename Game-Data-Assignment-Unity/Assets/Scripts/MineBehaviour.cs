﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MineBehaviour : MonoBehaviour
{

    public SpriteRenderer mySR;

    public bool tripped = false;

    public Color normal;
    public Color trippedCol;

    public int timesTripped =0 ;
    public Text tripDisplay;

    // Start is called before the first frame update
    void Start()
    {
        mySR = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        tripDisplay.text = timesTripped.ToString(); 

        if (tripped)
        {
            mySR.color = trippedCol;
        }
        else
        {
            mySR.color = normal;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        tripped = true;

        timesTripped++; 
    }
}
