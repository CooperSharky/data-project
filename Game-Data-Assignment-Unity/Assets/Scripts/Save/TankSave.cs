﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    [Serializable]
    public class Data : BaseData {

        //Here is the information that makes up the save state! 

        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    void Awake() {

        //Get the tank and make an instance of our Data class (which we just defined)

        tank = GetComponent<Tank>();
        data = new Data();
    }

    public override string Serialize() {
        //THIS TRIGGERS ON SAVE, FOR EACH OBJECT
        //To serialize, the Data class needs the prefab name plus all the relevant variables defined in the class. 

        //PrefabName is defined in the Save class
        data.prefabName = prefabName;

        //And for everything else... there's MasterCard. 
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;

        //JSON Utility conveniently puts all of our precious data in the JSON format, and then we pump it out as a string. 
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData) {

        //THIS TRIGGERS ON LOAD, for EACH OBJECT
        //Deserializing an instance of a saved prefab just means unloading the information fomr the JSON file, into the prefab. 
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}