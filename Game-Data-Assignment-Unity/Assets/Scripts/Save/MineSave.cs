﻿using System;
using UnityEngine;

[Serializable]
public class MineSave : Save
{

    public Data data;
    private MineBehaviour mine;
    private string jsonString;

    [Serializable]
    public class Data : BaseData
    {

        //Here is the information that makes up the save state! 

        public bool tripped;
        public Vector3 position;
        public int timesTripped; 
    }

    void Awake()
    {

        //Get the tank and make an instance of our Data class (which we just defined)

        mine = GetComponent<MineBehaviour>();
        data = new Data();
    }

    public override string Serialize()
    {
        //THIS TRIGGERS ON SAVE, FOR EACH OBJECT
        //To serialize, the Data class needs the prefab name plus all the relevant variables defined in the class. 

        //PrefabName is defined in the Save class
        data.prefabName = prefabName;
        data.position = mine.transform.position; 
        data.tripped = mine.tripped;
        data.timesTripped = mine.timesTripped; 

        //JSON Utility conveniently puts all of our precious data in the JSON format, and then we pump it out as a string. 
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData)
    {

        //THIS TRIGGERS ON LOAD, for EACH OBJECT
        //Deserializing an instance of a saved prefab just means unloading the information fomr the JSON file, into the prefab. 
        JsonUtility.FromJsonOverwrite(jsonData, data);
        mine.tripped = data.tripped;
        mine.transform.position = data.position;
        mine.timesTripped = data.timesTripped;
        mine.name = "MINE";
    }
}